package Database;
import java.io.Serializable;

public class Estados implements Serializable{
    private int id;
    private String nombreEstado;
    public Estados(){
        this.id = 0;
        this.nombreEstado = "";
    }
    public Estados(int id, String nombreEstado){
        this.id=id;
        this.nombreEstado=nombreEstado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }
}