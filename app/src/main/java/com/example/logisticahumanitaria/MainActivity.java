package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Database.DbEstados;
import Database.Estados;

public class MainActivity extends AppCompatActivity {
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private EditText txtEstado;
    private Estados savedEstado;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtEstado = (EditText) findViewById(R.id.txtEstado);

        DbEstados agenda = new DbEstados(MainActivity.this);
        agenda.openDatabase();
        if (agenda.allContactos().size() == 0) {
            String[] estados = {"Sinaloa", "Nayarit", "Zacatecas", "Veracruz", "Jalisco", "Nuevo León", "Tamaulipas", "Yucatán", "Oaxaca", "Quintana Roo"};
            for (int x = 0; x < 10; x++) {
                Estados e = new Estados();
                e.setNombreEstado(estados[x]);
                agenda.insertContacto(e);
            }
        }
        agenda.close();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean completo = true;
                if (txtEstado.getText().toString().equals("")){
                    txtEstado.setError("Introduce el nombre");
                    completo = false;
                }
                if (completo){
                    DbEstados source = new DbEstados(MainActivity.this);
                    source.openDatabase();
                    Estados oEstado = new Estados();
                    oEstado.setNombreEstado(txtEstado.getText().toString());
                    if (savedEstado == null){
                        source.insertContacto(oEstado);
                        Toast.makeText(MainActivity.this,"Estado Guardado!",Toast.LENGTH_SHORT).show();
                        limpiar();
                    } else {
                        source.updateContacto(oEstado,id);
                        Toast.makeText(MainActivity.this,"Estado Actualizado!", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                    source.close();
                }

            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(i, 0);
                limpiar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (Activity.RESULT_OK == resultCode){
            Estados estado = (Estados) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.getId();
            txtEstado.setText(estado.getNombreEstado());
        }
    }

    public void limpiar(){
        txtEstado.setText("");
        txtEstado.setError(null);
    }
}

